"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class GameRoom extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      GameRoom.belongsTo(models.User, {
        foreignKey: "playerOneId",
        as: "playerOne",
      });
      GameRoom.belongsTo(models.User, {
        foreignKey: "playerTwoId",
        as: "playerTwo",
      });
    }
  }
  GameRoom.init(
    {
      roomName: DataTypes.STRING,
      playerOneId: DataTypes.INTEGER,
      playerOneChoice: DataTypes.STRING,
      playerOneResult: DataTypes.STRING,
      playerTwoId: DataTypes.INTEGER,
      playerTwoChoice: DataTypes.STRING,
      playerTwoResult: DataTypes.STRING,
      roomStatus: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "GameRoom",
    }
  );
  return GameRoom;
};
