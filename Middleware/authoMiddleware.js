const jwtRule = require("jsonwebtoken");
// require("dotenv").config();

const authoMiddleware = async (req, res, next) => {
  //   cek apakah ada Authorization didalam header
  //   ambil key authorization dalam header
  const { authorization } = req.headers;
  
  // jika authorization undefined / null maka reject request
  if (authorization === undefined) {
    res.statusCode = 404;
    return res.json({ messages: "Unauthorized" });
  }

  try {
    // membagi access token
    const devidedToken = authorization.split(" ")[1];

    // cek apakah autorization valid, kalau tidak valid return Unauthorized, jika valid lanjutkan proses
    const token = await jwtRule.verify(
      devidedToken,
      "CobaanTujuh100114ZhRidhoanhu1!*",
    );

    // mengambil id dari token hasil login
    req.dataUserDariToken = token;

    next();
  } catch (error) {
    res.statusCode = 404;
    return res.json({ message: "Invalid token" });
  }
};

module.exports = authoMiddleware;
